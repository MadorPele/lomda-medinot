// window.onunload = function () {
//     sessionStorage.removeItem('alreadyVisited');
// }

$(function () {
    if (sessionStorage.getItem("alreadyVisited") === "true") {
        page3();
        $("#thirdPage").show();
        $("#firstPage").hide();
        $("#logoPakar").css({
            position: "absolute",
            left: "2%",
            top: "2%",
            height: "12vh",
            width: "6vw"
        });
        console.log("gg");
    }
    else {
        $("#startButton").click(page1);
        console.log("ggman");
    }
})

//Page 1
function page1() {
    $("#logoPakar").animate({
        position: "absolute",
        left: "2%",
        top: "2%",
        height: "12vh",
        width: "6vw"
    });
    $("#titleFirstPage").fadeOut();
    $("#startButton").fadeOut().off();
    $("#secondTitle").fadeIn(1500);
    $("#nextButton").fadeIn(1000).addClass("animate__swing animate__animated animate__delay-1s").on("click", page2);
}

//Page 2
function page2() {
    $("#nextButton").off();
    $("#secondTitle").addClass("animate__animated animate__backOutLeft").fadeOut(1000);
    $("#targetTitle").show().addClass("animate__animated animate__bounceInDown animate__delay-1s");
    setTimeout(function () {
        $("#targetText1").fadeIn(500);
    }, 2500)
    setTimeout(function () {
        $("#targetText2").fadeIn(500);
    }, 4500)
    setTimeout(function () {
        $("#nextButton").on("click", page3);
    }, 5000)
}

//Page 3
function page3() {
    sessionStorage.setItem("alreadyVisited", "true");
    $("#nextButton").off().fadeOut(1000);
    $("#targetTitle").removeClass("animate__animated animate__bounceInDown animate__delay-1s");
    $("#targetTitle").addClass("animate__animated animate__bounceOutUp");
    $("#targetText1").addClass("animate__animated animate__bounceOutRight");
    $("#targetText2").addClass("animate__animated animate__bounceOutLeft");
    $("#thirdPageTitle").show().addClass("animate__animated animate__bounceInDown animate__delay-1s");
    setTimeout(function () {
        $("#thirdPage").show();
        $(".imageText").fadeIn(1000);
        $(".imageClass").fadeIn(1000);
    }, 3000);
}

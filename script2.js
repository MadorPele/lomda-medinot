var textInfo = {
    circle1Text: 'הוקם במהלך מלחמת השחרור לאחר ההפצצה המצרית על תל אביב במאי 1948, שבמהלכה נהרגו ונפצעו אזרחים רבים, כשירות התגוננות בפני הרעשות, שנועד להגן על אזרחים מפני מתקפות אויב והיה אמון על הכנת האזרחים להתקפות אוויריות ועל פעולות חילוץ והצלה בזמן חירום. עם העברת חוק הג"א בכנסת (1951) שינה הארגון את שמו ל"שירות התגוננות אזרחית" (הג"א) ונקבעו תפקידיו וסמכויותיו כאחראי על היערכות העורף במדינת ישראל.',
    circle2Text: 'התקבלה החלטת ממשלה על הקמת מערך מל"ח ביוזמת ראש הממשלה דוד בן גוריון, שסבר כי גיוס תכוף של אמצעים וכוח אדם לצבא גורם למחסור של שירותים חיוניים במשק. לכן החליט כי יהיה גורם אחד שיכריע אילו אמצעי ייצור ומשאבים יוקצו לצבא בזמן מלחמה, ואילו לקיום המשק האזרחי והמשך הרציפות התפקודית שלו.',
    circle3Text: 'בשנה זו הוחלט על איחוד מפקדת הג"א עם מחלקת הגמ"ר - הגנה מרחבית. כתוצאה מכך הוקמה מקחל"ר (מפקדת קצין חיל ראשי להג"א ולהגמ"ר). עד תחילת שנות ה - 90 פעל מערך הג"א באמצעות שלוש מפקדות הכפופות לאלופי הפיקודים (מפקדת העורף בצפון, במרכז, ובדרום). ב 1988 ערך הג"א את תרגיל ההתגוננות הארצי הראשון במוסדות חינוך בישראל ומאז הפך תרגיל ההתגוננות הארצי לאירוע שמתקיים מדי שנה בכל בתי הספר במדינה.',
    circle4Text: 'לאחר מלחמת המפרץ הראשונה (1991) וירי טילי סקאד על העורף הישראלי חלה נקודת מפנה במערך העורף בישראל. בעקבות המלחמה הוחלט על הקמתו של חיל חדש שיבצע את משימותיו של שירות הג"א. בהתאם לזאת הוקם פיקוד העורף בצה"ל, שהפך לגורם האחראי להכנת האוכלוסייה האזרחית למצבי חירום ועל משימות הצלת חיים. עם הקמתו של פיקוד העורף בפבואר 1992 העבירה הממשלה את האחריות ואת הסמכויות של הג"א לפיקוד העורף.',
    circle5Text: 'מימוש תפיסת פקע"ר לקתה בחסר אל מול המציאות כפי שהשתקפה במהלך מלחמת לבנון השנייה. פקע"ר נכשל, לא פעל כנדרש בתחומים שונים ולא הבטיח את היערכותו של העורף בזמן חירום. בנוסף נמתחה ביקורת נרחבת על כך שמערך מל"ח לא הופעל, אף שיועד בדיוק למקרה שכזה.',
    circle6Text: 'יישום לקחי מלחמת לבנון השנייה (2006) הביא ל"הקמת פקע"ר בפעם השנייה" תחת ההבנה הנדרשת של חזרה להג"א ואימוץ תפיסת האחריות המרחיבה על הצלת החיים ועל החוסן האזרחי במלחמה. תפיסה זו תורגמה לפיתוח מרכיבי הג"א הקלאסיים הכוללים: התרעה, הסברה והדרכת אוכלוסייה, מיגון, חילוץ והצלה, מימוש שגרת חירום והכנת הציבור וגופי המדינה לחירום.',
    circle7Text: 'חטיבת החילוץ וההדרכה של פיקוד העורף (עד מרץ 2016 פעלה תחת השם נפת אלון, וכונתה גם "הנפה הסדירה" או "נפה 60") נודעה גם בשם פלח"צ (פלוגות חילוץ והצלה), היא חטיבת חילוץ והצלה קרבית סדירה. החטיבה מורכבת מארבעה גדודים: גדוד שחר (498), גדוד קדם (489), גדוד תבור (894) וגדוד רם (668). בנוסף לגדודים הלוחמים, חטיבת החילוץ מונה את גדוד הפיקוד (ענף המפקדים), הממונה על הכשרות הפיקוד והקצונה של כלל פיקוד העורף ואת בסיס האימון החטיבתי (בא"ח), הממונה על הכשרות הלוחמים.',
    circle8Text: 'בסוף שנת 2017 לאחר החלטה כי רח"ל מל"ח ופקע"ר אינם מסונכרנים, הנחה שר הביטחון אביגדור ליברמן להקים ועדת מומחים, במטרה לשפר באמצעות הסדרה ארגונית את המענה המבצעי לצרכי העורף בישראל במצב חירום ואת תהליכי המוכנות לחירום.'
}

var titleInfo = {
    circle1Title: 'הקמת הג"א',
    circle2Title: 'הקמת מערך מל"ח',
    circle3Title: 'הקמת מקחל"ר',
    circle4Title: 'הקמת פקע"ר',
    circle5Title: 'מלחמת לבנון השנייה',
    circle6Title: 'הקמת פקע"ר בפעם השנייה',
    circle7Title: 'הקמת חטיבת החילוץ',
    circle8Title: 'הקמת ועדת מזרחי'
}

var currentCircle = 1;
var done = false;

$(function () {
    $(".circle").hide();
    $("#circle1").css("display", "flex");
    $("#circle" + currentCircle).css("background-image", "url('./media/timeline/circle" + currentCircle + ".png')");
    $("#circle1").addClass("animate__animated animate__heartBeat animate__delay-1s");
    $("#nextButton").hide();
    $("#circle1").click(function () {
        $("#circle1").off("click");
        $("#infoBox").fadeIn(1000);
        showInfo(this.id.substring(6));
        setTimeout(function () {
            $("#nextButton").fadeIn(1000);
        }, 1500);
        $("#nextButton").on("click", nextPressed);
    });
});

// Shows the info (tilte + text) in the info box
function showInfo(circleNum) {
    $("#titleInfo").text(titleInfo["circle" + circleNum + "Title"]);
    $("#textInfo").text(textInfo["circle" + circleNum + "Text"]);
    $(".circle").css("opacity", "0.3");
    $("#circle" + circleNum).css("opacity", "1");
}

// When you press on the "next" button
function nextPressed() {
    // $("#circle" + currentCircle).css("background-color", "green");
    currentCircle++;
    $("#circle" + currentCircle).css("background-image", "url('./media/timeline/circle" + currentCircle + ".png')");
    if (currentCircle === 8) {
        // setTimeout(function () {
        //     $(".circle").css("background-color", "rgb(105, 169, 199)");
        // }, 1500);
        $("#nextButton").text("סיימתי!");
        $("#nextButton").click(function () {
            $("#nextButton").text("חזרה לדף הבית").click(function () {
                window.location.href = "main.html";
            });
            $("#nextButton").css("font-size", "1.3vw");
            // $("#circle" + currentCircle).css("background-color", "green");
            // $("#nextButton").fadeOut(400);
            // $(".circle").addClass("circleOver");
        });
        $(".circle").click(function () {
            showInfo(this.id.substring(6));
        });
    }
    if (currentCircle !== 9) {
        $("#timeline").animate({
            width: "+=10.5vw"
        });
        $("#circle" + currentCircle).css("display", "flex");
        showInfo(currentCircle);
    }
    // else {
    //     $(".circle").css("opacity", "0.5");
    //     $("#circle8").css("opacity", "1");
    //     done = true;
    // }
}